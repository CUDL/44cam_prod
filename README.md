Primo UI branding for the iDiscover service
===========================================

The service runs at http://idiscover.lib.cam.ac.uk. 


## Requirements

1.  Primo (of course)

2.  The Primo development environment. Start at https://github.com/ExLibrisGroup/primo-explore-devenv. Ensure you [comment out or remove the uglify statement](https://github.com/ExLibrisGroup/primo-explore-devenv/blob/master/gulp/tasks/buildCustomJs.js#L72).


## Installation

1.  Clone this repository as `44CAM_PROD` folder into the `<Primo dev env root>/primo-explore/custom` folder of the Primo dev env.

2.  Run `npm install` within the new folder which should create a `node_modules` directory and pull in all the other primo-explore-cam* packages. See https://bitbucket.org/account/user/CUDL/projects/PRIMO.

3.  Run `gulp run --view 44CAM_PROD --proxy https://idiscover.lib.cam.ac.uk`

4.  Go to http://localhost:8003/primo-explore/search?vid=44CAM_PROD&lang=en_US on your browser.


## Workflow

1.  Using the git workflow agreed upon, 
    * update the primo-explore-cam* repos except `primo-explore-cam-search-bar-tabs` which is a special case. See the [searchbar-tabs repo](https://bitbucket.org/CUDL/searchbar-tabs) for more information.
    * update the html/img content of 44cam_prod. css and js content should not be put in this package.

2.  Navigate to where you cloned `44CAM_PROD` on your local machine running the Primo dev env. 

3.  Run `npm update`. This will retrieve the latest commit from the master branch for each of the primo-explore-cam* repos. The repos to pull from are defined in [`package.json`](https://bitbucket.org/CUDL/44cam_prod/src/master/package.json).

4.  Run `gulp run --view 44CAM_PROD --proxy https://idiscover.lib.cam.ac.uk`

5.  Go to http://localhost:8003/primo-explore/search?vid=44CAM_PROD&lang=en_US on your browser.

6.  If all tests pass, then run `gulp create-package`, select `44CAM_PROD` from the list of options.

7.  A zip file called `44CAM_PROD.zip` is generated and stored in `<Primo dev env root>/packages`.

8.  Hand this file to the iDiscover team who will upload in our sandbox environment for testing.


